package com.example.cbk12.lab3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by cbk12 on 24.03.2018.
 */

public class BoardView extends View {
    private Ball ball;

    private int width;
    private int height;

    private int sideDifference;

    private Paint bkPaint;

    private boolean isColliding = false;

    private final float MODIFIER = -1.0f;

    public BoardView(Context context) {
        super(context);
        ball = new Ball();
        sideDifference = 5;

        bkPaint = new Paint();
        bkPaint.setColor(Color.YELLOW);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        height = h;
        width = w;

        ball.posX = w/ 2;
        ball.posY = h/ 2;
        ball.radius = w / 20;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(sideDifference, sideDifference, width - sideDifference, height- sideDifference, bkPaint);
        canvas.drawCircle(ball.posX, ball.posY, ball.radius, ball.paint);


    }

    public void update(float x, float y)
    {
        ball.updateSpeed(Math.toDegrees(x) * MODIFIER , Math.toDegrees(y)* MODIFIER);
        collision();
        invalidate();
    }



    public boolean collision()
    {
        boolean collision = false;

        if(ball.posX - ball.radius < sideDifference)
        {
            collision = true;
            ball.posX = sideDifference + ball.radius;
            //ball.speedX = -ball.speedX;

        }
        else if (ball.posX + ball.radius > width - sideDifference)
        {
            collision = true;
            ball.posX = width - sideDifference -ball.radius;
            //ball.speedX = -ball.speedX;
        }
        if(ball.posY - ball.radius < sideDifference)
        {
            collision = true;
            ball.posY = sideDifference + ball.radius;
            //ball.speedY = -ball.speedY;
        }
        else if(ball.posY + ball.radius > height - sideDifference)
        {
            collision = true;
            ball.posY = height - sideDifference - ball.radius;
            //ball.speedY = -ball.speedY;
        }

        if(collision && !isColliding)
        {
            ((MainActivity)getContext()).onCollision();
            isColliding = true;
        }
        else if(!collision)
        {
            isColliding = false;
        }

        return collision;
    }
}
