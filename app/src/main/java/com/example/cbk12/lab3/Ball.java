package com.example.cbk12.lab3;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by cbk12 on 24.03.2018.
 */

public class Ball {
    public int posX;
    public int posY;

    public double speedX;
    public double speedY;

    public final float MAX_SPEED = 3.0f;

    public int radius;

    public Paint paint;

    public Ball()
    {
        paint = new Paint();
        paint.setColor(Color.CYAN);

        speedX = 0.0f;
        speedY = 0.0f;
    }

    void updateSpeed(double x, double y)
    {
        speedX = x;
        speedY = y;
        if(speedX > MAX_SPEED)
        {
            speedX = MAX_SPEED;
        }
        if(speedY > MAX_SPEED)
        {
            speedY = MAX_SPEED;
        }
        posX += (int) speedX;
        posY += (int) speedY;
    }



}
